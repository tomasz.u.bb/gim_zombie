﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AVariable<T> : ScriptableObject where T : IComparable
{
    public Action<T> OnChanged = delegate { };

    [SerializeField]
    protected T m_Value;
    public T Value
    {
        get
        {
            return m_Value;
        }

        set
        {
            if(m_Value.CompareTo(value) != 0)
            {
                m_Value = value;
                OnChanged.Invoke(m_Value);
            }
        }
    }
}
