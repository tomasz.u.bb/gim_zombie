﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character Commands", menuName = "Bindings/Character Commands")]
public class CharacterCommands : ScriptableObject {
    public bool IsShootRequested = false;
    public Vector3 ShootTarget = Vector3.zero;
}
