﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Int Variable", menuName = "Variables/Int")]
public class IntVariable : AVariable<int>
{

}
