﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyCharacter : ACharacter
{
    public FloatVariable SelfDestructRadius;
    public FloatVariable SelfDestructDamage;
    public FloatVariable ExplosionRadius;
    public ParticleSystem DestroyParticle;

    Transform Target { get; set; }

    NavMeshAgent m_Agent;
    NavMeshAgent Agent
    {
        get
        {
            if(m_Agent == null)
            {
                m_Agent = GetComponent<NavMeshAgent>();
            }

            return m_Agent;
        }
    }

    private void OnEnable()
    {
        CurrentHP.OnChanged += OnHPChanged;
    }

    private void OnDisable()
    {
        CurrentHP.OnChanged -= OnHPChanged;
    }

    private void Update()
    {
        if(Target == null)
        {
            return;
        }

        if(Vector3.Distance(Target.position, transform.position) <= SelfDestructRadius.Value)
        {
            CurrentHP.Value = 0.0f;
        }
    }

    public void SetTarget(Transform target)
    {
        Target = target;
        if (Agent.isOnNavMesh)
        {
            Agent.SetDestination(target.position);
        }
    }

    void OnHPChanged(float hp)
    {
        if (hp <= 0.0f)
        {
            var inRange = Physics.OverlapSphere(transform.position, ExplosionRadius.Value);
            for (int i = 0; i < inRange.Length; i++)
            {
                var character = inRange[i].GetComponent<ACharacter>();
                if(character != null)
                {
                    character.Damage(SelfDestructDamage.Value);
                }
            }

            Instantiate<ParticleSystem>(DestroyParticle, transform.position, transform.rotation).Play();
            Destroy(gameObject);
        }
    }
}
