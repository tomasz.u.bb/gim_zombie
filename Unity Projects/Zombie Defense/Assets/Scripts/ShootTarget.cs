﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShootTarget : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public CharacterCommands Receiver;


    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        PointerEventData pData = (PointerEventData)eventData;
        if (pData.button == PointerEventData.InputButton.Left)
        {
            if (pData.pointerCurrentRaycast.worldPosition != Vector3.zero)
            {
                Receiver.ShootTarget = pData.pointerCurrentRaycast.worldPosition;
            }
        }
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        PointerEventData pData = (PointerEventData)eventData;
        if (pData.button == PointerEventData.InputButton.Left)
        {
            Receiver.IsShootRequested = true;
            Receiver.ShootTarget = pData.pointerCurrentRaycast.worldPosition;
        }
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        PointerEventData pData = (PointerEventData)eventData;
        if (pData.button == PointerEventData.InputButton.Left)
        {
            Receiver.IsShootRequested = false;
        }
    }
}
