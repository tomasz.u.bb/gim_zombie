﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public List<Transform> SpawnPoints = new List<Transform>();
    public EnemyCharacter EnemyPrefab;
    public Transform EnemyTarget;
    public int EnemiesToSpawn = 1;

    private void Start()
    {
        for (int i = 0; i < EnemiesToSpawn; i++)
        {
            Transform spawnPoint = SpawnPoints[Random.Range(0, SpawnPoints.Count)];
            var enemy = Instantiate<EnemyCharacter>(EnemyPrefab, spawnPoint.position, spawnPoint.rotation);
            enemy.SetTarget(EnemyTarget);
        }
    }
}
