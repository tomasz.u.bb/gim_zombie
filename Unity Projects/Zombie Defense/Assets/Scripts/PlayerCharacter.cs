﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : ACharacter
{
    public CharacterCommands Commands;
    public Gun Gun;
    public ParticleSystem DestroyParticle;

    [SerializeField]
    FloatVariable m_CurrentHP;

    protected override FloatVariable CurrentHP
    {
        get
        {
            return m_CurrentHP;
        }
    }

    private void OnEnable()
    {
        CurrentHP.OnChanged += OnHPChanged;
    }

    private void OnDisable()
    {
        CurrentHP.OnChanged -= OnHPChanged;
    }

    void Update () {
		if(Commands.IsShootRequested)
        {
            Vector3 lookTarget = Commands.ShootTarget;
            lookTarget.y = transform.position.y;
            transform.LookAt(lookTarget);
            Gun.Trigger();
        }
	}

    void OnHPChanged(float hp)
    {
        if(hp <= 0.0f)
        {
            Instantiate<ParticleSystem>(DestroyParticle, transform.position, transform.rotation).Play();
            Destroy(gameObject);
        }
    }
}
