﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public CharacterCommands Commands;
    public ParticleSystem FireParticle;
    public ParticleSystem HitParticle;
    [Space]
    public float Cooldown = 0.1f;
    [Space]
    public IntVariable ClipSize;
    public IntVariable CurrentAmmo;
    public FloatVariable ReloadTime;
    public FloatVariable TimeToReload;
    public FloatVariable DamagePerHit;

    float m_LastShootTime = 0.0f;

    private void Start()
    {//reset/reload
        CurrentAmmo.Value = ClipSize.Value;
        TimeToReload.Value = 0.0f;
    }

    private void Update()
    {
        if(TimeToReload.Value > 0.0f)
        {
            TimeToReload.Value -= Time.deltaTime;

            if(TimeToReload.Value <= 0.0f)
            {
                TimeToReload.Value = 0.0f;
                CurrentAmmo.Value = ClipSize.Value;
            }
        }
    }

    public void Trigger()
    {
        if(CanShoot())
        {
            Shoot();
        }
    }

    void Shoot()
    {
        m_LastShootTime = Time.realtimeSinceStartup;

        FireParticle.Play();

        RaycastHit hitInfo;
        if(Physics.Raycast(transform.position, Commands.ShootTarget - transform.position, out hitInfo))
        {
            Instantiate<ParticleSystem>(HitParticle, Commands.ShootTarget, Quaternion.LookRotation(hitInfo.normal)).Play();

            var character = hitInfo.collider.GetComponent<ACharacter>();
            if(character != null)
            {
                character.Damage(DamagePerHit.Value);
            }
        }
        else
        {
            Instantiate<ParticleSystem>(HitParticle, Commands.ShootTarget, Quaternion.LookRotation(Commands.ShootTarget - transform.position)).Play();
        }

        CurrentAmmo.Value--;
        if(CurrentAmmo.Value <= 0)
        {
            TimeToReload.Value = ReloadTime.Value;
        }
    }

    bool CanShoot()
    {
        bool noAmmoInClip = CurrentAmmo.Value == 0;
        noAmmoInClip |= TimeToReload.Value > 0.0f;
        if (noAmmoInClip)
        {
            return false;
        }

        if((m_LastShootTime + Cooldown) <= Time.realtimeSinceStartup)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
