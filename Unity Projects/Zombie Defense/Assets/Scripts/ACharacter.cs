﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ACharacter : MonoBehaviour {
    public FloatVariable MaxHP;

    protected virtual FloatVariable CurrentHP
    {
        get;set;
    }

    protected virtual void Awake()
    {
        if(CurrentHP == null)
        {
            CurrentHP = ScriptableObject.CreateInstance<FloatVariable>();
        }

        CurrentHP.Value = MaxHP.Value;
    }
    
    public void Damage(float damage)
    {
        CurrentHP.Value = Mathf.Clamp(CurrentHP.Value - damage, 0.0f, MaxHP.Value);
    }
}
